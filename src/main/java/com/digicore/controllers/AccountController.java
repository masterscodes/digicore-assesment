package com.digicore.controllers;


import com.digicore.http.requests.DepositRequest;
import com.digicore.http.requests.WithdrawRequest;
import com.digicore.http.response.APIResponse;
import com.digicore.http.response.AccountSummaryResponse;
import com.digicore.http.response.TransactionResponse;
import com.digicore.models.Transaction;
import com.digicore.services.intefaces.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/v1")
@RequiredArgsConstructor
public @RestController
class AccountController {
    private final AccountService accountService;


    @GetMapping("/account-info/{accountNumber}")
    public @ResponseBody  APIResponse<AccountSummaryResponse> getAccountInfo(@PathVariable String accountNumber) {
        return accountService.getAccountSummary(accountNumber);
    }

    @GetMapping("/account-statement/{accountNumber}")
    public List<Transaction> getAccountStatement(@PathVariable String accountNumber) {
        return accountService.getAccountStatement(accountNumber);
    }

    @PostMapping("/deposit")
    public @ResponseBody
    TransactionResponse doDeposit(@RequestBody DepositRequest depositRequest) {
        return accountService.makeDeposit(depositRequest);
    }

    @PostMapping("/withdraw")
    public @ResponseBody
    TransactionResponse doDeposit(@RequestBody WithdrawRequest depositRequest) {
        return accountService.makeWithdrawal(depositRequest);
    }
}
