package com.digicore.services.intefaces;

import com.digicore.http.requests.CreateAccountRequest;
import com.digicore.http.requests.DepositRequest;
import com.digicore.http.requests.LoginRequest;
import com.digicore.http.requests.WithdrawRequest;
import com.digicore.http.response.*;
import com.digicore.models.Transaction;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface AccountService extends UserDetailsService {
    APIResponse<AccountSummaryResponse> getAccountSummary(String accountNumber);
    List<Transaction> getAccountStatement(String accountNumber);
    CreateAccountResponse createAccount(CreateAccountRequest request);

    TransactionResponse makeDeposit(DepositRequest request);

    TransactionResponse makeWithdrawal(WithdrawRequest request);

    LoginResponse doLogin(LoginRequest loginRequest);
}
