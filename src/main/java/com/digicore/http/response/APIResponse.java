package com.digicore.http.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class APIResponse<T> {
    private int responseCode;
    private boolean success;
    private String message;
    private T data;
}
