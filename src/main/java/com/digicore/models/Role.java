package com.digicore.models;


import com.digicore.models.enums.ROLE;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;

@Setter
@Slf4j
@Getter
public class Role implements GrantedAuthority {

    private ROLE role;

    @Override
    public String getAuthority() {
        return role.name();
    }
}
