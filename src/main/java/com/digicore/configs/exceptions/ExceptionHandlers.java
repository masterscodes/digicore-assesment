package com.digicore.configs.exceptions;

import com.digicore.http.response.ExceptionResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDate;


@RestControllerAdvice
public class ExceptionHandlers  {

    @ExceptionHandler(value = DigiCoreRESTException.class)
    public ResponseEntity<Object> handleException(DigiCoreRESTException ex){
        return ResponseEntity.badRequest().body(
                new ExceptionResponse(ex.getMessage(), HttpStatus.BAD_REQUEST, LocalDate.now())
        );
    }

    @ExceptionHandler(value = UsernameNotFoundException.class)
    public ResponseEntity<Object> handleException(UsernameNotFoundException ex){
        return ResponseEntity.badRequest().body(
                new ExceptionResponse(ex.getMessage(), HttpStatus.BAD_REQUEST, LocalDate.now())
        );
    }

}
