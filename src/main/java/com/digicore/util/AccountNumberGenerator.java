package com.digicore.util;

import java.util.Random;

public class AccountNumberGenerator {

    public static String generate(){
        Random random = new Random();
        int rdNum;
        String m[] = new String[10];
        for(int i=0; i<10;i++){
            rdNum = random.nextInt(10);
            m[i] = Integer.toString(rdNum);
        }
        return m[0]+m[1]+m[2]+m[3]+m[4]+m[5]+m[6]+m[7]+m[8]+m[9];
    }


}
