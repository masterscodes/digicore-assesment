package com.digicore.http.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DepositRequest implements Serializable {
    private String accountNumber;
    private Double amount;
}
