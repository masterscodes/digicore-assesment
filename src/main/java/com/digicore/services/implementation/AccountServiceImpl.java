package com.digicore.services.implementation;

import com.digicore.configs.security.JWTUtil;
import com.digicore.http.requests.CreateAccountRequest;
import com.digicore.http.requests.DepositRequest;
import com.digicore.http.requests.LoginRequest;
import com.digicore.http.requests.WithdrawRequest;
import com.digicore.http.response.*;
import com.digicore.models.Account;
import com.digicore.models.Transaction;
import com.digicore.models.enums.TransactionType;
import com.digicore.services.intefaces.AccountService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
public class AccountServiceImpl implements AccountService {
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JWTUtil jwtUtil;

    private final List<Account> accounts = new ArrayList<>();

    @Override
    public Account loadUserByUsername(String s) throws UsernameNotFoundException {
        System.out.println(accounts);
        return accounts.stream().filter(account -> account.getAccountNumber().equalsIgnoreCase(s)).findFirst().orElseThrow(()->new UsernameNotFoundException("This account does not exist"));
    }


    public @Override APIResponse<AccountSummaryResponse> getAccountSummary(String accountNumber) {
        Account account =  loadUserByUsername(accountNumber);
        var response =  new APIResponse<AccountSummaryResponse>();
        response.setSuccess(true);
        response.setResponseCode(200);
        response.setData(modelMapper.map(account, AccountSummaryResponse.class));
        response.setMessage("Account Information");
        return response;
    }

    @Override
    public List<Transaction> getAccountStatement(String accountNumber) {
        return (loadUserByUsername(accountNumber)).getTransactions();
    }

    @Override
    public CreateAccountResponse createAccount(CreateAccountRequest request) {
        if (request.getInitialDeposit() < 500) return new CreateAccountResponse(400, false,"Initial deposit should be minimum #500:00");
        var filteredAccount = accounts.stream().filter(account -> account.getAccountName().equalsIgnoreCase(request.getAccountName())).findFirst();
        if (filteredAccount.isPresent()) return new CreateAccountResponse(400, false,"Account name already exists");

        Account newAccount = modelMapper.map(request, Account.class);
        newAccount.setAccountPassword(passwordEncoder.encode(request.getAccountPassword()));
        accounts.add(newAccount);

        makeDeposit(new DepositRequest(newAccount.getAccountNumber(), request.getInitialDeposit()));

        System.out.println(newAccount);

        return new CreateAccountResponse(200,true, "Account Successfully created!");
    }

    @Override
    public TransactionResponse makeDeposit(DepositRequest request) {
        Double amount = request.getAmount();
        Account account = loadUserByUsername(request.getAccountNumber());

        var newAccountBalance = getAccountBalance(account, request.getAmount(), TransactionType.DEPOSIT);
        if (verifyTransactionAmount(amount) != null)  return verifyTransactionAmount(amount);

        Transaction transaction = new Transaction (LocalDate.now(),  TransactionType.DEPOSIT,String.format("Amount deposit of %s was successful", request.getAmount()),request.getAmount(), newAccountBalance );
        account.getTransactions().add(transaction);
        account.setBalance(newAccountBalance);
        return new TransactionResponse(200,true,"Amount deposit successful");
    }

    @Override
    public TransactionResponse makeWithdrawal(WithdrawRequest request) {
        var amount = request.getWithdrawnAmount();
        if (verifyTransactionAmount(request.getWithdrawnAmount()) != null)  return verifyTransactionAmount(request.getWithdrawnAmount());

        Account account = loadUserByUsername(request.getAccountNumber());

        //authorize
        if (!passwordEncoder.matches(request.getAccountPassword(), account.getPassword()))  return new TransactionResponse(403, false, "Unauthorized");

        //perform withdrqwal
        var balanceAfterWithdrawal = getAccountBalance(account, amount, TransactionType.WITHDRAWAL);
        if (balanceAfterWithdrawal < 500)   return new TransactionResponse(400, false,"Amount limit exceeded");

        Transaction withdrawal = new Transaction(LocalDate.now(),TransactionType.WITHDRAWAL,String.format("Amount withdrawal of %s was successful", request.getWithdrawnAmount()), request.getWithdrawnAmount(),balanceAfterWithdrawal);

        account.getTransactions().add(withdrawal);
        account.setBalance(account.getBalance() - request.getWithdrawnAmount());

        return new TransactionResponse(200,true,"Transaction successful, thank you for banking with us!");

    }

    @Override
    public LoginResponse doLogin(LoginRequest loginRequest) {


        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(
                        loginRequest.getAccountNumber(), loginRequest.getAccountPassword()
                ));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetails loggedInUser = loadUserByUsername(loginRequest.getAccountNumber());

        final String jwtToken = jwtUtil.generateToken(loggedInUser);

        return new LoginResponse(jwtToken, true);
    }

    private Double getAccountBalance(Account account, Double amount, TransactionType transactionType){
        return
                transactionType.name().equals("DEPOSIT") ? account.getBalance() + amount : account.getBalance() - amount;
    }

    private TransactionResponse verifyTransactionAmount(Double amount){
        if (amount < 1 || amount > 1_000_000)   return new TransactionResponse(400, false,"Amount limit exceeded");
        return null;
    }
}
