package com.digicore.models.enums;

public enum TransactionType {
    WITHDRAWAL, DEPOSIT
}
